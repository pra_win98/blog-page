import React, { useState } from "react";
import { GoogleLogout } from "react-google-login";
import { useSelector, useDispatch } from "react-redux";
import {
  selectUserData,
  selectSignedIn,
  setSignedIn,
  setUserData,
  setInput,
} from "../features/userSlice";

const Navbar = () => {
  const [inputField, setInputField] = useState("tech");
  const isSignedIn = useSelector(selectSignedIn);
  const userData = useSelector(selectUserData);
  const dispatch = useDispatch();
  const handleClick = (e) => {
    e.preventDefault();
    dispatch(setInput(inputField));
  };
  const logout = () => {
    dispatch(setSignedIn(false));
    dispatch(setUserData(null));
  };
  return (
    <div className="navbar">
      <div className="navbar__header">Blog App</div>
      {isSignedIn && (
        <div className="blog__search">
          <input
            type="text"
            className="search"
            placeholder="Search for a blog"
            value={inputField}
            onChange={(e) => setInputField(e.target.value)}
          />
          <button className="submit" onClick={handleClick}>
            Search
          </button>
        </div>
      )}
      {isSignedIn ? (
        <div className="navbar__userdata">
          <img
            src={userData?.imageUrl}
            alt={userData?.name}
            className="avatar"
          />
          <h1 className="signedIn">{userData?.givenName}</h1>
          <GoogleLogout
            clientId={process.env.REACT_APP_CLIENT_ID}
            render={(renderProps) => (
              <button
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
                className="logout__button"
              >
                Logout
              </button>
            )}
            onLogoutSuccess={logout}
          />
        </div>
      ) : (
        <h1 className="notSignedIn">User not available 😞</h1>
      )}
    </div>
  );
};

export default Navbar;
