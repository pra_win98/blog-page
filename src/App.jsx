import React from "react";
import { useSelector } from "react-redux";
import Homepage from "./components/Homepage";
import Navbar from "./components/Navbar";
import Blog from "./components/Blog";
import { selectSignedIn } from "./features/userSlice";

const App = () => {
  const isSignedIn = useSelector(selectSignedIn);
  return (
    <div>
      <Navbar />
      <Homepage />
      {isSignedIn && <Blog />}
    </div>
  );
};

export default App;
