import React, { useEffect, useState } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { selectUserInput, setBlogData } from "../features/userSlice";
const Blog = () => {
  const searchInput = useSelector(selectUserInput);
  const blogUrl = `https://gnews.io/api/v4/search?q=${searchInput}&token=${process.env.REACT_APP_NEWS_API_TOKEN}`;
  const dispatch = useDispatch();
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios
      .get(blogUrl)
      .then((res) => {
        dispatch(setBlogData(res.data));
        setBlogs(res.data);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  }, [searchInput, dispatch, blogUrl]);
  return (
    <div className="blog__page">
      <h1 className="blog__page__header">Blogs</h1>
      {loading && <h1 className="loading">Loading....</h1>}
      <div className="blogs">
        {blogs?.articles?.map((blog, i) => (
          <a rel="noreferrer" className="blog" target="_blank" href={blog.url}>
            <img src={blog.image} key={i} alt="" />
            <div>
              <h3 className="sourceName">
                <span>{blog.source.name}</span>
                <span>{blog.publishedAt}</span>
              </h3>
              <h1>{blog.title}</h1>
              <p>{blog.description}</p>
            </div>
          </a>
        ))}

        {blogs?.totalArticles === 0 && (
          <h1 className="no_blogs">
            No blogs available 😞. Search something else to read blogs on
            greatest platform.
          </h1>
        )}
      </div>
    </div>
  );
};

export default Blog;
